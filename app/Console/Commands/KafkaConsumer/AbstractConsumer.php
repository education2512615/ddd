<?php

namespace App\Console\Commands\KafkaConsumer;

use AvroIOException;
use Carbon\Exceptions\Exception;
use FlixTech\AvroSerializer\Objects\RecordSerializer;
use FlixTech\SchemaRegistryApi\Registry\BlockingRegistry;
use FlixTech\SchemaRegistryApi\Registry\Cache\AvroObjectCacheAdapter;
use FlixTech\SchemaRegistryApi\Registry\CachedRegistry;
use FlixTech\SchemaRegistryApi\Registry\PromisingRegistry;
use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Junges\Kafka\Facades\Kafka;
use Junges\Kafka\Message\ConsumedMessage;
use Junges\Kafka\Message\Deserializers\AvroDeserializer;
use Junges\Kafka\Message\KafkaAvroSchema;
use Junges\Kafka\Message\Registry\AvroSchemaRegistry;

abstract class AbstractConsumer extends Command
{
    /**
     * @throws \Exception
     * @throws Exception
     */
    public function handle(): void
    {
        $arrTopic = [$this->getTopic()];
        $deserializer = $this->AVRODeserializer();

        $consumer = Kafka::createConsumer(
            topics: $arrTopic,
            groupId: $this->getGroupId(),
            brokers: $this->getBrokers()
        )->usingDeserializer($deserializer);

        $closure = $this->handleMessage(...);
        $consumer = $consumer->withHandler(static function (ConsumedMessage $message) use ($closure) {
            $closure($message->getBody());
        })->build();

        $consumer->consume();
    }

    abstract protected function handleMessage($message): void;

    abstract protected function getTopic(): string;

    abstract protected function getSchemaName(): string;

    abstract protected function getGroupId(): string;

    protected function getBrokers(): string
    {
        return config('kafka.brokers');
    }

    protected function getBaseUriSerializer()
    {
        return config('kafka.base_uri_serializer');
    }

    /**
     * @throws AvroIOException
     */
    public function AVRODeserializer(): AvroDeserializer
    {
        $cachedRegistry = new CachedRegistry(
            new BlockingRegistry(
                new PromisingRegistry(
                    new Client(['base_uri' => $this->getBaseUriSerializer()])
                )
            ),
            new AvroObjectCacheAdapter()
        );

        $registry = new AvroSchemaRegistry($cachedRegistry);
        $recordSerializer = new RecordSerializer($cachedRegistry);

        $registry->addBodySchemaMappingForTopic(
            $this->getTopic(),
            new KafkaAvroSchema($this->getSchemaName())
        );

        return new AvroDeserializer($registry, $recordSerializer);
    }
}
