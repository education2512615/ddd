<?php

namespace App\Console\Commands\KafkaConsumer;


class CartCommand extends AbstractConsumer
{
    protected $signature = 'kafka:cart';

    protected function getTopic(): string
    {
        return config('kafka.topics.cart');
    }

    protected function getSchemaName(): string
    {
        return config('kafka.schema_avro.cart');
    }

    protected function getGroupId(): string
    {
        return config('kafka.consumer_group_id.assign_rc');
    }

    protected function handleMessage($message): void
    {
        $this->line(json_encode($message));
    }
}
