<?php


namespace App\Http\Common;


enum HttpStatusCode: int
{
    /***********************************************************************************************
     * 2xx                                                                                         *
     ***********************************************************************************************/
    /** @var int Trả về thành công cho những phương thức GET, PUT, PATCH hoặc DELETE. */
    case NO_ERROR = 200;

    /** @var int Trả về khi một Resouce vừa được tạo thành công. */
    case CREATE_SUCCESS = 201;

    /** @var int Trả về khi Resource xoá thành công. */
    case NO_CONTENT = 204;

    /***********************************************************************************************
     * 3xx                                                                                         *
     ***********************************************************************************************/
    /** @var int Client có thể sử dụng dữ liệu cache. */
    case NOT_MODIFIED = 304;

    /***********************************************************************************************
     * 4xx                                                                                         *
     ***********************************************************************************************/
    /** @var int Request không hợp lệ, VALIDATOIN_ERROR */
    case BAD_REQUEST = 400;

    /** @var int Request cần có auth. */
    case UNAUTHEN = 401;

    /** @var int bị từ chối không cho phép. */
    case FORBIDDEN = 403;

    /** @var int Không tìm thấy resource từ URI */
    case NOT_FOUND = 404;

    /** @var int Phương thức không cho phép với user hiện tại. */
    case METHOD_NOT_ALLOWED = 405;

    /** @var int Resource không còn tồn tại, Version cũ đã không còn hỗ trợ. */
    case GONE = 410;

    /** @var int Không hỗ trợ kiểu Resource này. */
    case UNSUPPORTED_MEDIA_TYPE = 415;

    /** @var int  Unprocessable Entity */
    case UNPROCESSABLE_ENTITY = 422;

    /** @var int Request bị từ chối do bị giới hạn */
    case TOO_MANY_REQUESTS = 429;

    /***********************************************************************************************
     * 5xx                                                                                         *
     ***********************************************************************************************/
    /** @var int Lỗi hệ thống */
    case SYSTEM_ERROR = 500;

    /** @var int Dịch vụ không khả dụng */
    case SERVICE_UNAVAILABLE = 511;
}
