<?php

namespace App\Http\Controllers\API\V1;

use App\Helper\Response;
use App\Http\Controllers\Controller;
use DTL\Application\Order\Command\CreateOrderCommand;
use DTL\Application\Order\DTO\OrderItemInfo;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use League\Tactician\CommandBus;

class OrderController extends Controller
{
    public function __construct(private readonly CommandBus $commandBus)
    {
    }

    public function create(Request $request): ?JsonResponse
    {
        try {
            $data = $request->all();

            $orderItemInfos = [];
            foreach ($data['products'] as $product) {
                $orderItemInfos[] = new OrderItemInfo(
                    productId: $product['product_id'],
                    quantity: $product['quantity'],
                    voucher: $product['voucher']
                );
            }

            $command = new CreateOrderCommand(
                customerId: $data['customer_id'],
                orderItems: $orderItemInfos,
                note: $data['note'],
                orderVoucher: $data['voucher']
            );

            $this->commandBus->handle($command);

            return Response::sendResponse();
        } catch (\Exception $exception) {
            report($exception);
            return Response::sendError(
                message: $exception->getMessage()
            );
        }
    }
}
