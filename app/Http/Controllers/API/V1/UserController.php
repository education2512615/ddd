<?php

namespace App\Http\Controllers\API\V1;

use App\Helper\Response;
use App\Http\Controllers\Controller;
use DTL\Application\User\GetUserByIdQuery;
use DTL\Domain\Model\User\User;
use Illuminate\Http\JsonResponse;
use League\Tactician\CommandBus;

class UserController extends Controller
{
    public function __construct(private readonly CommandBus $queryBus)
    {
    }

    public function detail(int $id): ?JsonResponse
    {
        try {
            $query = new GetUserByIdQuery(id: $id);
            /** @var User $user */
            $user = $this->queryBus->handle($query);
            return Response::sendResponse(data: [$user->toArray()]);
        } catch (\Exception $exception) {
            return Response::sendError(message: $exception->getMessage());
        }
    }
}
