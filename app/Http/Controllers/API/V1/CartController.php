<?php

namespace App\Http\Controllers\API\V1;

use App\Helper\Response;
use App\Http\Controllers\Controller;
use DTL\Application\Cart\Command\CreatCartCommand;
use DTL\Application\Cart\Command\CreateOrderCartCommand;
use DTL\Application\Cart\Command\DeleteCartCommand;
use DTL\Application\Cart\Command\UpdateCartCommand;
use DTL\Application\Cart\DTO\CartItemInfo;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use League\Tactician\CommandBus;

class CartController extends Controller
{
    public function __construct(private readonly CommandBus $commandBus)
    {
    }

    public function store(Request $request): ?JsonResponse
    {
        try {
            $customerId = Auth::id();

            $data = $request->all();

            $command = new CreatCartCommand(
                customerId: $customerId,
                productId: $data['id'],
                quantity: $data['quantity'],
                note: $data['note']
            );

            $this->commandBus->handle($command);

            return Response::sendResponse();
        } catch (\Exception $exception) {
            report($exception);
            return Response::sendError(
                message: $exception->getMessage()
            );
        }
    }

    public function update(Request $request): ?JsonResponse
    {
        try {
            $customerId = Auth::id();

            $data = $request->all();

            $command = new UpdateCartCommand(
                customerId: $customerId,
                productId: $data['id'],
                quantity: $data['quantity'],
                note: $data['note']
            );

            $this->commandBus->handle($command);

            return Response::sendResponse();
        } catch (\Exception $exception) {
            report($exception);
            return Response::sendError(
                message: $exception->getMessage()
            );
        }
    }

    public function delete(Request $request): ?JsonResponse
    {
        try {
            $customerId = Auth::id();

            $data = $request->all();

            $command = new DeleteCartCommand(
                customerId: $customerId,
                productId: $data['id'],
                quantity: $data['quantity'],
                note: $data['note']
            );

            $this->commandBus->handle($command);

            return Response::sendResponse();
        } catch (\Exception $exception) {
            report($exception);
            return Response::sendError(
                message: $exception->getMessage()
            );
        }
    }

    public function order(Request $request): ?JsonResponse
    {
        try {
            $customerId = Auth::id();

            $data = $request->all();

            $cartItemInfos = [];
            foreach ($data['products'] as $product) {
                $cartItemInfos[] = new CartItemInfo(
                    productId: $product['product_id'],
                    quantity: $product['quantity'],
                    itemVoucher: $product['voucher']
                );
            }

            $command = new CreateOrderCartCommand(
                customerId: $customerId,
                cartItems: $cartItemInfos,
                note: $data['note'],
                voucher: $data['voucher']
            );

            $this->commandBus->handle($command);

            return Response::sendResponse();
        } catch (\Exception $exception) {
            report($exception);
            return Response::sendError(
                message: $exception->getMessage()
            );
        }
    }
}
