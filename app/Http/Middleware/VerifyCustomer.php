<?php

namespace App\Http\Middleware;

use App\Helper\Response;
use App\Http\Common\HttpStatusCode;
use Closure;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class VerifyCustomer
{
    public function handle(Request $request, Closure $next): JsonResponse
    {
        $token = $request->bearerToken();
        if (!$this->verifyToken($token)) {
            return Response::sendError(
                message: 'Token không hợp lệ',
                errors: 'No error',
                httpStatus: HttpStatusCode::UNPROCESSABLE_ENTITY
            );
        }
        return $next($request);
    }

    private function verifyToken(?string $token): bool
    {
//        if (!$token) {
//            return false;
//        }


        return true;
    }
}
