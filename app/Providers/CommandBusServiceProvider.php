<?php

namespace App\Providers;

use DTL\Infrastructer\Middleware\TransactionMiddleware;
use Illuminate\Support\ServiceProvider;
use League\Tactician\CommandBus;
use League\Tactician\Handler\CommandHandlerMiddleware;
use League\Tactician\Handler\CommandNameExtractor\ClassNameExtractor;
use League\Tactician\Handler\Locator\InMemoryLocator;
use League\Tactician\Handler\MethodNameInflector\HandleInflector;

class CommandBusServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(CommandBus::class, function () {
            $middlewares = [];

            $middlewares[] = new TransactionMiddleware();
            $middlewares[] = app()->get(CommandHandlerMiddleware::class);

            return new CommandBus($middlewares);
        });

        $app = $this->app;
        $this->app->bind(CommandHandlerMiddleware::class, static function () use ($app) {
            return new CommandHandlerMiddleware(
                new ClassNameExtractor(),
                $app->get(InMemoryLocator::class),
                new HandleInflector()
            );
        });
    }
}
