<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use League\Tactician\Handler\Locator\InMemoryLocator;

class CommandHandlerServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->bind(InMemoryLocator::class, function () {
            $locator = new InMemoryLocator();

            foreach (config('command_bus.command_handlers') as $command => $handler) {
                $locator->addHandler(app()->make($handler), $command);
            }
            return $locator;
        });
    }
}
