<?php

namespace App\Providers;

use DTL\Domain\Event\Cart\CartWasDeleted;
use DTL\Domain\Event\Cart\CartWasUpdated;
use DTL\Domain\Event\Cart\OrderCartWasCreated;
use DTL\Domain\Event\Order\OrderWasCreated;
use Illuminate\Foundation\Support\Providers\EventServiceProvider;
use DTL\Domain\Event\Cart\CartWasCreated;

class DomainEventServiceProvider extends EventServiceProvider
{

    protected $listen = [
        CartWasCreated::class => [],
        CartWasUpdated::class => [],
        CartWasDeleted::class => [],
        OrderCartWasCreated::class => [],
        OrderWasCreated::class => []
    ];
}
