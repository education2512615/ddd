<?php

namespace App\Providers;

use DTL\Domain\Model\Cart\CartRepository;
use DTL\Domain\Model\Customer\CustomerRepository;
use DTL\Domain\Model\Inquiry\InquiryRepository;
use DTL\Domain\Model\Invoice\InvoiceRepository;
use DTL\Domain\Model\Order\OrderRepository;
use DTL\Domain\Model\Product\ProductRepository;
use DTL\Domain\Model\User\UserRepository;
use DTL\Domain\Model\VirtualAccount\VirtualAccountRepository;
use DTL\Domain\Model\Voucher\VoucherRepository;
use DTL\Infrastructer\Persistence\Mysql\Cart\MysqlCartRepository;
use DTL\Infrastructer\Persistence\Mysql\Customer\MysqlCustomerRepository;
use DTL\Infrastructer\Persistence\Mysql\Inquiry\MysqlInquiryRepository;
use DTL\Infrastructer\Persistence\Mysql\Invoice\MysqlInvoiceRepository;
use DTL\Infrastructer\Persistence\Mysql\Order\MysqlOrderRepository;
use DTL\Infrastructer\Persistence\Mysql\Product\MysqlProductRepository;
use DTL\Infrastructer\Persistence\Mysql\User\MysqlUserRepository;
use DTL\Infrastructer\Persistence\Mysql\VirtualAccount\MysqlVirtualAccountRepository;
use DTL\Infrastructer\Persistence\Mysql\Voucher\MysqlVoucherRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->bind(CustomerRepository::class, MysqlCustomerRepository::class);
        $this->app->bind(ProductRepository::class, MysqlProductRepository::class);
        $this->app->bind(CartRepository::class, MysqlCartRepository::class);
        $this->app->bind(OrderRepository::class, MysqlOrderRepository::class);
        $this->app->bind(VoucherRepository::class, MysqlVoucherRepository::class);

        $this->app->bind(InquiryRepository::class, MysqlInquiryRepository::class);
        $this->app->bind(InvoiceRepository::class, MysqlInvoiceRepository::class);
        $this->app->bind(VirtualAccountRepository::class, MysqlVirtualAccountRepository::class);

        $this->app->bind(UserRepository::class, MysqlUserRepository::class);
    }
}
