<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use League\Tactician\Handler\Locator\InMemoryLocator;

class QueryHandlerServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->bind(InMemoryLocator::class, function () {
            $locator = new InMemoryLocator();

            foreach (config('query_bus.query_handlers') as $query => $handler) {
                $locator->addHandler(app()->make($handler), $query);
            }
            return $locator;
        });
    }
}
