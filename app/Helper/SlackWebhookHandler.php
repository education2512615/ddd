<?php

namespace App\Helper;

use Monolog\Handler\MissingExtensionException;
use Monolog\Handler\SlackWebhookHandler as BaseSlackWebhookHandler;
use Monolog\Logger;

class SlackWebhookHandler extends BaseSlackWebhookHandler
{
    /**
     * @throws MissingExtensionException
     */
    public static function make(array $config): self
    {
        return new self(
            $config['url'],
            $config['channel'] ?? null,
            $config['username'] ?? 'Laravel',
            $config['attachment'] ?? true,
            $config['emoji'] ?? ':boom:',
            $config['short'] ?? false,
            $config['context'] ?? true,
            $config['level'] ?? Logger::DEBUG,
            $config['bubble'] ?? true,
            $config['exclude_fields'] ?? []
        );
    }
}
