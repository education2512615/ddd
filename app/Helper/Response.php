<?php

namespace App\Helper;

use App\Http\Common\HttpStatusCode;
use Illuminate\Http\JsonResponse;

class Response
{
    public static function sendResponse(
        array $data = [],
        string $message = '',
        $httpStatus = HttpStatusCode::NO_ERROR
    ): JsonResponse {
        return response()->json([
            'status' => 'success',
            'message' => $message,
            'data' => $data
        ], $httpStatus->value);
    }

    public static function sendError(
        string $message = '',
        string $errors = '',
        $httpStatus = HttpStatusCode::SYSTEM_ERROR
    ): JsonResponse {
        return response()->json([
            'status' => 'fail',
            'message' => $message,
            'errors' => $errors,
        ], $httpStatus->value);
    }

}
