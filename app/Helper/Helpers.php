<?php

namespace App\Helper;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class Helpers
{
    public static function callAPIOrderService($method, $url, $params)
    {
        $options = [];

        if ($method === 'GET') {
            $options['query'] = $params;
        } else {
            $options['form_params'] = $params;
        }
        $httpClient = new Client([
                'request.options' => array(
                    'exceptions' => false
                )
            ]
        );
        try {
            $response = $httpClient->request($method, $url, $options);
            $response = $response->getBody()->getContents();
            return json_decode($response, true, 512, JSON_THROW_ON_ERROR);
        } catch (RequestException $exception) {
            return json_decode($exception->getResponse()->getBody(), true, 512, JSON_THROW_ON_ERROR);
        }
    }
}
