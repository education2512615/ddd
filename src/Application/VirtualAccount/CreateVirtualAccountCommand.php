<?php

namespace DTL\Application\VirtualAccount;

class CreateVirtualAccountCommand
{
    public function __construct(
        public readonly int $customerId,
        public readonly int $invoiceId,
        public readonly string $vaNumber,
        public readonly int $type
    ) {
    }
}
