<?php

namespace DTL\Application\VirtualAccount;

use DTL\Domain\Event\VirtualAccount\VirtualAccountWasCreated;
use DTL\Domain\Model\Customer\CustomerRepository;
use DTL\Domain\Model\Invoice\InvoiceRepository;
use DTL\Domain\Model\VirtualAccount\Status;
use DTL\Domain\Model\VirtualAccount\Type;
use DTL\Domain\Model\VirtualAccount\VirtualAccount;
use DTL\Domain\Model\VirtualAccount\VirtualAccountNumber;
use DTL\Domain\Model\VirtualAccount\VirtualAccountRepository;

class CreateVirtualAccountHandler
{
    public function __construct(
        private readonly VirtualAccountRepository $virtualAccountRepository,
        private readonly InvoiceRepository $invoiceRepository,
        private readonly CustomerRepository $customerRepository
    ) {
    }

    public function handle(CreateVirtualAccountCommand $command): void
    {
        $customer = $this->customerRepository->findById($command->customerId);
        $invoice = $this->invoiceRepository->findById($command->invoiceId);

        $virtualAccount = VirtualAccount::fromPrimitives(
            customer: $customer,
            virtualAccountNumber: VirtualAccountNumber::fromString($command->vaNumber),
            invoice: $invoice,
            type: Type::from($command->type),
            status: Status::VALID
        );

        $this->virtualAccountRepository->save($virtualAccount);

        event(new VirtualAccountWasCreated($virtualAccount));
    }
}
