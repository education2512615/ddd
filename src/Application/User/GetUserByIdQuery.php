<?php

namespace DTL\Application\User;

class GetUserByIdQuery
{
    public function __construct(public readonly int $id)
    {
    }
}
