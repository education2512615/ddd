<?php

namespace DTL\Application\User;

use DTL\Domain\Model\User\User;
use DTL\Domain\Model\User\UserRepository;

class GetUserByIdHandler
{
    public function __construct(private readonly UserRepository $userRepository)
    {
    }

    public function handle(GetUserByIdQuery $query): User
    {
        return $this->userRepository->getById(id: $query->id);
    }
}
