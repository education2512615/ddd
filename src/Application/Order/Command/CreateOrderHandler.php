<?php

namespace DTL\Application\Order\Command;

use DTL\Application\Order\DTO\OrderItemInfo;
use DTL\Domain\Event\Order\OrderWasCreated;
use DTL\Domain\Model\Customer\CustomerRepository;
use DTL\Domain\Model\Order\Note;
use DTL\Domain\Model\Order\Order;
use DTL\Domain\Model\Order\OrderItem;
use DTL\Domain\Model\Order\OrderRepository;
use DTL\Domain\Model\Order\Quantity;
use DTL\Domain\Model\Order\VAT;
use DTL\Domain\Model\Product\ProductRepository;
use DTL\Domain\Model\Voucher\ScopeVoucher;
use DTL\Domain\Model\Voucher\VoucherRepository;

class CreateOrderHandler
{
    public function __construct(
        private readonly OrderRepository $orderRepository,
        private readonly CustomerRepository $customerRepository,
        private readonly ProductRepository $productRepository,
        private readonly VoucherRepository $voucherRepository
    ) {
    }

    public function handle(CreateOrderCommand $command): void
    {
        $customer = $this->customerRepository->findById(id: $command->customerId);

        $orderItems = [];
        foreach ($command->orderItems as $item) {
            $orderItems[] = $this->getOrderItem($item);
        }

        $order = Order::fromPrimitives(
            customer: $customer,
            orderItems: $orderItems,
            note: Note::fromString($command->note),
            vat: VAT::VAT_2023
        );

        if ($command->orderVoucher) {
            $orderVoucher = $this->voucherRepository->findByCode($command->orderVoucher);

            if ($orderVoucher->getScopeVoucher() !== ScopeVoucher::ORDER) {
                throw new \LogicException('Mã giảm giá cho đơn hàng không hợp lệ: ' . $command->orderVoucher);
            }

            $order->setOrderVoucher(orderVoucher: $orderVoucher);
        }

        $this->orderRepository->create(order: $order);

        $this->productRepository->updateProductStock(order: $order);

        $this->voucherRepository->updateVoucherStatus(order: $order);

        event(new OrderWasCreated(order: $order));
    }

    private function getOrderItem(OrderItemInfo $itemInfo): OrderItem
    {
        $product = $this->productRepository->findById(id: $itemInfo->productId);

        if ($product->getStock() < $itemInfo->quantity) {
            throw new \LogicException('Số lượng hàng hóa còn lại không đủ');
        }

        $quantity = Quantity::fromInt($itemInfo->quantity);

        $orderItem = OrderItem::fromValues(product: $product, quantity: $quantity);

        if ($itemInfo->voucher) {
            $productVoucher = $this->voucherRepository->findByCode($itemInfo->voucher);

            if ($productVoucher->getScopeVoucher() !== ScopeVoucher::ITEM) {
                throw new \LogicException('Mã giảm giá cho sản phẩm không hợp lệ: ' . $itemInfo->voucher);
            }

            $orderItem->setProductVoucher(productVoucher: $productVoucher);
        }

        return $orderItem;
    }
}
