<?php

namespace DTL\Application\Order\Command;

use DTL\Application\Order\DTO\OrderItemInfo;
use DTL\Application\TransactionCommand;

class CreateOrderCommand implements TransactionCommand
{
    public function __construct(
        public readonly int $customerId,
        /**
         * @var OrderItemInfo[]
         */
        public readonly array $orderItems,
        public readonly string $note,
        public readonly ?string $orderVoucher
    ) {
    }
}
