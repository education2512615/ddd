<?php

namespace DTL\Application\Order\DTO;

class OrderItemInfo
{
    public function __construct(
        public readonly int $productId,
        public readonly int $quantity,
        public readonly ?string $voucher
    ) {
    }
}
