<?php

namespace DTL\Application\Inquiry;

class InquiryCommand
{
    public function __construct(
        public readonly int $amount,
        public readonly string $vaNumber,
        public readonly string $messageId,
        public readonly string $sender,
        public readonly string $receiver,
        public readonly string $createdDate
    ) {
    }
}
