<?php

namespace DTL\Application\Inquiry;

use DTL\Domain\Model\Inquiry\Detail\CreatedDate;
use DTL\Domain\Model\Inquiry\Detail\Detail;
use DTL\Domain\Model\Inquiry\Detail\MessageId;
use DTL\Domain\Model\Inquiry\Detail\Receiver;
use DTL\Domain\Model\Inquiry\Detail\Sender;
use DTL\Domain\Model\Inquiry\Inquiry;
use DTL\Domain\Model\Inquiry\InquiryRepository;
use DTL\Domain\Model\VirtualAccount\VirtualAccountRepository;

class InquiryHandler
{
    public function __construct(
        private readonly InquiryRepository $inquiryRepository,
        private readonly VirtualAccountRepository $virtualAccountRepository
    ) {
    }

    public function handle(InquiryCommand $command): void
    {
        $virtualAccount = $this->virtualAccountRepository->findByVirtualAccountNumber($command->vaNumber);
        $inquiryDetail = Detail::fromValues(
            messageId: MessageId::fromString($command->messageId),
            createdDate: CreatedDate::fromString($command->createdDate),
            sender: Sender::fromString($command->sender),
            receiver: Receiver::fromString($command->receiver)
        );

        $inquiry = Inquiry::fromPrimitives(virtualAccount: $virtualAccount, detail: $inquiryDetail);

        if ($command->amount) {
            $hasInquiryInfo = $this->inquiryRepository->checkInquiryInfo($inquiry);

            if (!$hasInquiryInfo) {
                throw new \LogicException('Số tiền không hợp lệ');
            }
        }
    }
}
