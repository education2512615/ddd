<?php

namespace DTL\Application\Cart\DTO;

class CartItemInfo
{
    public function __construct(
        public readonly int $productId,
        public readonly int $quantity,
        public readonly ?string $itemVoucher
    ) {
    }
}
