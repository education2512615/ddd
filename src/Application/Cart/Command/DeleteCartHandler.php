<?php

namespace DTL\Application\Cart\Command;

use DTL\Domain\Event\Cart\CartWasDeleted;

class DeleteCartHandler extends AbstractCartHandler
{
    public function handle(DeleteCartCommand $command): void
    {
        $cart = $this->cartRepository->findById(productId: $command->productId, customerId: $command->customerId);

        $this->cartRepository->delete($cart);

        event(new CartWasDeleted(cart: $cart));
    }
}
