<?php

namespace DTL\Application\Cart\Command;

use DTL\Domain\Model\Cart\Cart;
use DTL\Domain\Model\Cart\CartItem;
use DTL\Domain\Model\Cart\CartRepository;
use DTL\Domain\Model\Cart\Note;
use DTL\Domain\Model\Cart\Quantity;
use DTL\Domain\Model\Customer\CustomerRepository;
use DTL\Domain\Model\Product\ProductRepository;

abstract class AbstractCartHandler
{
    public function __construct(
        protected readonly CartRepository $cartRepository,
        protected readonly CustomerRepository $customerRepository,
        protected readonly ProductRepository $productRepository
    ) {
    }

    public function makeCart(AbstractCartCommand $command): Cart
    {
        $customer = $this->customerRepository->findById(id: $command->customerId);

        $cartItems[] = $this->getProduct($command);

        $note = Note::fromString($command->note);

        return Cart::fromPrimitives(customer: $customer, cartItems: $cartItems, note: $note);
    }

    private function getProduct(AbstractCartCommand $command): CartItem
    {
        $product = $this->productRepository->findById(id: $command->productId);

        if ($command instanceof UpdateCartCommand) {
            $cartData = $this->cartRepository->findById(
                productId: $command->productId,
                customerId: $command->customerId
            );

            $existQuantity = 0;
            foreach ($cartData->getCartItems() as $cartItem) {
                $existQuantity += $cartItem->getQuantity()->getValue();
            }

            $quantity = Quantity::fromInt($command->quantity + $existQuantity);
        } else {
            $quantity = Quantity::fromInt($command->quantity);
        }

        return CartItem::fromValues($product, $quantity);
    }
}
