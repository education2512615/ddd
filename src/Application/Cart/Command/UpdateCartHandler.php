<?php

namespace DTL\Application\Cart\Command;

use DTL\Domain\Event\Cart\CartWasUpdated;
use DTL\Domain\Model\Cart\Quantity;

class UpdateCartHandler extends AbstractCartHandler
{
    public function handle(UpdateCartCommand $command): void
    {
        $cart = $this->cartRepository->findById(productId: $command->productId, customerId: $command->customerId);

        $updated = false;
        foreach ($cart->getCartItems() as $item) {
            if (!$updated && $item->getProduct()->getProductId() === $command->productId
                && $item->getQuantity()->getValue() !== $command->quantity) {
                $item->setQuantity(Quantity::fromInt($command->quantity));
                $updated = true;
            }
        }

        if ($updated) {
            $this->cartRepository->update($cart);

            event(new CartWasUpdated(cart: $cart));
        }
    }
}
