<?php

namespace DTL\Application\Cart\Command;

use DTL\Application\TransactionCommand;

abstract class AbstractCartCommand implements TransactionCommand
{
    public function __construct(
        public readonly int $customerId,
        public readonly int $productId,
        public readonly int $quantity,
        public readonly ?string $note
    ) {
    }
}
