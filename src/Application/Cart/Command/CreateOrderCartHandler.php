<?php

namespace DTL\Application\Cart\Command;

use DTL\Application\Cart\DTO\CartItemInfo;
use DTL\Domain\Event\Cart\OrderCartWasCreated;
use DTL\Domain\Model\Cart\Cart;
use DTL\Domain\Model\Cart\CartItem;
use DTL\Domain\Model\Cart\CartRepository;
use DTL\Domain\Model\Cart\Note;
use DTL\Domain\Model\Cart\Quantity;
use DTL\Domain\Model\Customer\CustomerRepository;
use DTL\Domain\Model\Product\ProductRepository;

class CreateOrderCartHandler
{
    public function __construct(
        protected readonly CartRepository $cartRepository,
        protected readonly CustomerRepository $customerRepository,
        protected readonly ProductRepository $productRepository
    ) {
    }

    public function handle(CreateOrderCartCommand $command): void
    {
        $cart = $this->makeCart($command);

        $this->cartRepository->order(cart: $cart);

        event(new OrderCartWasCreated(cart: $cart));
    }

    private function makeCart(CreateOrderCartCommand $command): Cart
    {
        $customer = $this->customerRepository->findById(id: $command->customerId);

        $cartItems = [];
        foreach ($command->cartItems as $cartItem) {
            $cartItems[] = $this->getProduct($cartItem);
        }

        $note = Note::fromString($command->note);

        $cart = Cart::fromPrimitives(customer: $customer, cartItems: $cartItems, note: $note);

        $cart->setVoucher(voucher: $command->voucher);

        return $cart;
    }

    private function getProduct(CartItemInfo $command): CartItem
    {
        $product = $this->productRepository->findById(id: $command->productId);

        $quantity = Quantity::fromInt($command->quantity);

        $cartItem = CartItem::fromValues($product, $quantity);

        $cartItem->setItemVoucher(itemVoucher: $command->itemVoucher);

        return $cartItem;
    }
}
