<?php

namespace DTL\Application\Cart\Command;

use DTL\Application\Cart\DTO\CartItemInfo;

class CreateOrderCartCommand
{
    public function __construct(
        public readonly int $customerId,
        /**
         * @var CartItemInfo[]
         */
        public readonly array $cartItems,
        public readonly string $note,
        public readonly ?string $voucher
    ) {
    }

}
