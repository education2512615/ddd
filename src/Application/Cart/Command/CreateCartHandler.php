<?php

namespace DTL\Application\Cart\Command;

use DTL\Domain\Event\Cart\CartWasCreated;

class CreateCartHandler extends AbstractCartHandler
{
    public function handle(CreatCartCommand $command): void
    {
        $cart = $this->makeCart($command);

        $this->cartRepository->create(cart: $cart);

        event(new CartWasCreated(cart: $cart));
    }
}
