<?php

namespace DTL\Infrastructer\Persistence\Mysql\Product;

use App\Models\Product as ProductEloquent;
use DTL\Domain\Model\Order\Order;
use DTL\Domain\Model\Product\Price;
use DTL\Domain\Model\Product\Product;
use DTL\Domain\Model\Product\ProductId;
use DTL\Domain\Model\Product\ProductRepository;

class MysqlProductRepository implements ProductRepository
{
    public function findById(int $id): Product
    {
        // TODO: Implement findById() method.
        $productEloquent = ProductEloquent::find($id);

        if (!$productEloquent) {
            throw new \LogicException('Không tìm thấy sản phẩm');
        }

        $product = Product::fromValue(productId: ProductId::fromInt($id));

        $product->setPrice(price: Price::fromString($productEloquent->price));

        $product->setStock($productEloquent->stock);

        return $product;
    }

    public function updateProductStock(Order $order): void
    {
        // TODO: Implement updateProductStock() method.
        foreach ($order->getOrderItems() as $orderItem) {
            $productEloquent = ProductEloquent::find($orderItem->getProduct()->getProductId()->getValue());
            if ($productEloquent) {
                $productEloquent->stock = $productEloquent->quantity - $orderItem->getQuantity()->getValue();
                $productEloquent->save();
            }
        }
    }
}
