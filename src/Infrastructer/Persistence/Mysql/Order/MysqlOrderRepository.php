<?php

namespace DTL\Infrastructer\Persistence\Mysql\Order;

use App\Models\OrderItem;
use Carbon\Carbon;
use DTL\Domain\Model\Order\Order;
use DTL\Domain\Model\Order\OrderRepository;
use App\Models\Order as OrderEloquent;

class MysqlOrderRepository implements OrderRepository
{
    public function create(Order $order): void
    {
        // TODO: Implement create() method.
        $orderEloquent = OrderEloquent::create([
            'customer_id' => $order->getCustomer()->getCustomerId()->getValue(),
            'note' => $order->getNote()->getValue(),
            'VAT' => $order->getVat()->value,
            'voucher' => $order->getOrderVoucher()?->getCode()->getValue()
        ]);

        $orderItems = [];
        $totalOrderPrice = 0;

        foreach ($order->getOrderItems() as $item) {
            $price = $item->getProduct()->getPrice()->getValue();
            $realPrice = $price - $item->getProductVoucher()->getCash()->getValue();
            $totalOrderPrice += $realPrice;
            $orderItems[] = [
                'product_id' => $item->getProduct()->getProductId()->getValue(),
                'quantity' => $item->getQuantity()->getValue(),
                'price' => $price,
                'voucher' => $item->getProductVoucher()?->getCode()->getValue(),
                'real_price' => $realPrice,
                'order_id' => $orderEloquent->id,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ];
        }

        $totalOrderPrice -= $order->getOrderVoucher()?->getCash()->getValue();
        $totalOrderPrice += $totalOrderPrice * $order->getVat()->value / 100;

        OrderItem::insert($orderItems);

        OrderEloquent::where('id', $orderEloquent->id)->update(['total_price', $totalOrderPrice]);

        $order->setId($orderEloquent->id);
    }
}
