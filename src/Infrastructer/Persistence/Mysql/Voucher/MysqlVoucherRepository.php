<?php

namespace DTL\Infrastructer\Persistence\Mysql\Voucher;

use DTL\Domain\Model\Order\Order;
use DTL\Domain\Model\Voucher\Cash;
use DTL\Domain\Model\Voucher\Code;
use DTL\Domain\Model\Voucher\ScopeVoucher;
use DTL\Domain\Model\Voucher\Status;
use DTL\Domain\Model\Voucher\Voucher;
use DTL\Domain\Model\Voucher\VoucherRepository;
use App\Models\Voucher as VoucherEloquent;

class MysqlVoucherRepository implements VoucherRepository
{
    public function findByCode(string $code): Voucher
    {
        // TODO: Implement findByCode() method.
        $voucherEloquent = VoucherEloquent::query()->where('code', $code)->where(
            'status',
            Status::UN_USED->value
        )->first();

        if (!$voucherEloquent) {
            throw new \LogicException('Voucher không tồn tại');
        }

        if ($voucherEloquent->expired_at < now()){
            throw new \LogicException('Voucher đã hết hạn sử dụng');
        }

        if ($voucherEloquent->scope === ScopeVoucher::ITEM) {
            $scopeVoucher = ScopeVoucher::ITEM;
        } else {
            $scopeVoucher = ScopeVoucher::ORDER;
        }

        return Voucher::fromPrimitives(
            code: Code::fromString($code),
            cash: Cash::fromString($voucherEloquent->cash),
            expiredAt: $voucherEloquent->expriedAt,
            scopeVoucher: $scopeVoucher,
            status: Status::UN_USED
        );
    }

    public function updateVoucherStatus(Order $order): void
    {
        // TODO: Implement updateVoucherStatus() method.
        $vouchers = [];
        if ($order->getOrderVoucher()?->getCode()->getValue()) {
            $vouchers[] = $order->getOrderVoucher()?->getCode()->getValue();
        }

        foreach ($order->getOrderItems() as $item) {
            if ($item->getProductVoucher()?->getCode()->getValue()) {
                $vouchers[] = $item->getProductVoucher()?->getCode()->getValue();
            }
        }

        VoucherEloquent::query()->whereIn('code', $vouchers)->update(['status' => Status::USED]);
    }
}
