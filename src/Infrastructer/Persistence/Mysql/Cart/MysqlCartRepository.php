<?php

namespace DTL\Infrastructer\Persistence\Mysql\Cart;

use App\Helper\Helpers;
use Carbon\Carbon;
use DTL\Domain\Model\Cart\Cart;
use DTL\Domain\Model\Cart\CartItem;
use DTL\Domain\Model\Cart\CartRepository;
use App\Models\Cart as CartEloquent;
use DTL\Domain\Model\Cart\Note;
use DTL\Domain\Model\Cart\Quantity;
use DTL\Domain\Model\Customer\Customer;
use DTL\Domain\Model\Customer\CustomerId;
use DTL\Domain\Model\Product\Product;
use DTL\Domain\Model\Product\ProductId;
use Exception;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\DB;
use SRP\Domain\Model\Order\Exception\CreateOrderException;
use SRP\Domain\Model\Order\SellType;

class MysqlCartRepository implements CartRepository
{

    public function create(Cart $cart): void
    {
        // TODO: Implement create() method.
        $carts = [];
        foreach ($cart->getCartItems() as $cartItem) {
            $carts[] = [
                'product_id' => $cartItem->getProduct()->getProductId()->getValue(),
                'quantity' => $cartItem->getQuantity()->getValue(),
                'customer_id' => $cart->getCustomer()->getCustomerId()->getValue(),
                'note' => $cart->getNote()->getValue(),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ];
        }

        CartEloquent::insert($carts);
    }

    public function update(Cart $cart): void
    {
        // TODO: Implement update() method.
        foreach ($cart->getCartItems() as $cartItem) {
            CartEloquent::query()->where('product_id', $cartItem->getProduct()->getProductId()->getValue())
                ->where('customer_id', $cart->getCustomer()->getCustomerId()->getValue())
                ->update(
                    ['quantity' => $cartItem->getQuantity()->getValue(), 'note' => $cart->getNote()->getValue()]
                );
        }
    }

    public function delete(Cart $cart): void
    {
        // TODO: Implement delete() method.
        foreach ($cart->getCartItems() as $cartItem) {
            CartEloquent::query()->where('product_id', $cartItem->getProduct()->getProductId()->getValue())
                ->where('customer_id', $cart->getCustomer()->getCustomerId()->getValue())
                ->delete();
        }
    }

    public function findById(int $productId, int $customerId): Cart
    {
        // TODO: Implement findById() method.
        $cartEloquent = CartEloquent::query()->where('product_id', $productId)
            ->where(
                'customer_id',
                $customerId
            )->first();

        if (!$cartEloquent) {
            throw new \LogicException('Không tồn tại sản phẩm trong giỏ hàng');
        }

        $cartItems[] = CartItem::fromValues(
            product: Product::fromValue(ProductId::fromInt($productId)),
            quantity: Quantity::fromInt(
                $cartEloquent->quantity
            )
        );

        return Cart::fromPrimitives(
            customer: Customer::fromValue(CustomerId::fromInt($customerId)),
            cartItems: $cartItems,
            note: Note::fromString($cartEloquent->note ?? '')
        );
    }

    public function order(Cart $cart): void
    {
        // TODO: Implement order() method.
        try {
            $url = config('domains.order_service') . '/api/orders';

            $products = [];
            foreach ($cart->getCartItems() as $cartItem) {
                $products[] = [
                    'product_id' => $cartItem->getProduct()->getProductId()->getValue(),
                    'quantity' => $cartItem->getQuantity()->getValue(),
                    'voucher' => $cartItem->getItemVoucher()
                ];
            }

            $param = [
                'customer_id' => $cart->getCustomer()->getCustomerId()->getValue(),
                'voucher' => $cart->getVoucher(),
                "note" => $cart->getNote()->getValue(),
                'products' => $products
            ];

            DB::beginTransaction();
            $this->delete($cart);
            $response = Helpers::callAPIOrderService('POST', $url, $param);
            if ($response['success']) {
                DB::commit();
            }
            DB::rollBack();
            throw new \LogicException($response['message']);
        } catch (GuzzleException $exception) {
            throw new \LogicException($exception->getMessage());
        } catch (\JsonException $e) {
            throw new \LogicException($e->getMessage());
        }
    }
}
