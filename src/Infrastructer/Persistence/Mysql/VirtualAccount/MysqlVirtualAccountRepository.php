<?php

namespace DTL\Infrastructer\Persistence\Mysql\VirtualAccount;

use DTL\Domain\Model\Customer\Customer;
use DTL\Domain\Model\Customer\CustomerId;
use DTL\Domain\Model\Invoice\Invoice;
use DTL\Domain\Model\Invoice\InvoiceId;
use DTL\Domain\Model\VirtualAccount\Status;
use DTL\Domain\Model\VirtualAccount\Type;
use DTL\Domain\Model\VirtualAccount\VirtualAccount;
use DTL\Domain\Model\VirtualAccount\VirtualAccountNumber;
use DTL\Domain\Model\VirtualAccount\VirtualAccountRepository;
use App\Models\VirtualAccount as VirtualAccountEloquent;

class MysqlVirtualAccountRepository implements VirtualAccountRepository
{

    public function findByVirtualAccountNumber(string $virtualNumber): VirtualAccount
    {
        // TODO: Implement findByVirtualAccountNumber() method.
        $virtualAccountEloquent = VirtualAccountEloquent::query()->where('va_number', $virtualNumber)->first();

        if (!$virtualAccountEloquent) {
            throw new \LogicException('Virtual Account không hợp lệ');
        }

        return VirtualAccount::fromPrimitives(
            customer: Customer::fromValue(
                customerId: CustomerId::fromInt(
                    $virtualAccountEloquent->customer_id
                )
            ),
            virtualAccountNumber: VirtualAccountNumber::fromString($virtualAccountEloquent->va_number),
            invoice: Invoice::fromValues(invoiceId: InvoiceId::fromInt($virtualAccountEloquent->invoice_id)),
            type: Type::from($virtualAccountEloquent->type),
            status: Status::from($virtualAccountEloquent->status)
        );
    }

    public function save(VirtualAccount $virtualAccount): void
    {
        // TODO: Implement save() method.
        VirtualAccountEloquent::updateOrCreate([
            'customer_id' => $virtualAccount->getCustomer()->getCustomerId()->getValue(),
            'invoice_id' => $virtualAccount->getInvoice()->getInvoiceId()->getValue(),
            'va_number' => $virtualAccount->getVirtualAccountNumber()->getValue(),
            'type' => $virtualAccount->getType()->value,
            'status' => $virtualAccount->getStatus()->value
        ]);
    }
}
