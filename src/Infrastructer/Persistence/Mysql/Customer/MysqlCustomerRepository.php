<?php

namespace DTL\Infrastructer\Persistence\Mysql\Customer;

use DTL\Domain\Model\Customer\Customer;
use DTL\Domain\Model\Customer\CustomerId;
use DTL\Domain\Model\Customer\CustomerRepository;
use App\Models\Customer as CustomerEloquent;

class MysqlCustomerRepository implements CustomerRepository
{
    public function findById(int $id): Customer
    {
        // TODO: Implement findById() method.
        $customerEloquent = CustomerEloquent::find($id);

        if (!$customerEloquent) {
            throw new \LogicException('Không tìm thấy người dùng');
        }

        return Customer::fromValue(customerId: CustomerId::fromInt($id));
    }
}
