<?php

namespace DTL\Infrastructer\Persistence\Mysql\User;

use DTL\Domain\Model\User\BirthDate;
use DTL\Domain\Model\User\Email;
use DTL\Domain\Model\User\User;
use DTL\Domain\Model\User\UserId;
use DTL\Domain\Model\User\UserName;
use DTL\Domain\Model\User\UserRepository;

class MysqlUserRepository implements UserRepository
{

    public function getById(int $id): User
    {
        // TODO: Implement getById() method.
        return User::fromPrimitives(
            userId: new UserId(1),
            userName: UserName::fromString('ThoLD'),
            email: Email::fromString('leduytho93@gmail.com'),
            birthDate: BirthDate::fromValue('1993-10-02')
        );
    }
}
