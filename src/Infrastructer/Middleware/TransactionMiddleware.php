<?php

namespace DTL\Infrastructer\Middleware;

use DTL\Application\TransactionCommand;
use Illuminate\Support\Facades\DB;
use League\Tactician\Middleware;

class TransactionMiddleware implements Middleware
{
    public function execute($command, callable $next)
    {
        if (!$command instanceof TransactionCommand) {
            return $next($command);
        }

        return DB::transaction(static function () use ($command, $next) {
            return $next($command);
        });
    }
}
