<?php

namespace DTL\Domain\Model\Customer;

use Webmozart\Assert\Assert;

class CustomerId
{
    private function __construct(
        private readonly int $value
    ) {
        Assert::integer($value, "Customer không hợp lệ");
    }

    public static function fromInt(?int $value): self
    {
        return new self($value);
    }

    /**
     * @return int
     */
    public function getValue(): int
    {
        return $this->value;
    }

}
