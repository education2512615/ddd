<?php

namespace DTL\Domain\Model\Customer;

class Customer
{
    private function __construct(private readonly CustomerId $customerId)
    {
    }

    public static function fromValue(CustomerId $customerId): self
    {
        return new self($customerId);
    }

    /**
     * @return CustomerId
     */
    public function getCustomerId(): CustomerId
    {
        return $this->customerId;
    }


}
