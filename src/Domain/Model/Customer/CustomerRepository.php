<?php

namespace DTL\Domain\Model\Customer;

interface CustomerRepository
{
    public function findById(int $id): Customer;
}
