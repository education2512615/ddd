<?php

namespace DTL\Domain\Model\Invoice;

class Invoice
{
    private function __construct(private readonly InvoiceId $invoiceId)
    {
    }

    public static function fromValues(InvoiceId $invoiceId): self
    {
        return new self($invoiceId);
    }

    /**
     * @return InvoiceId
     */
    public function getInvoiceId(): InvoiceId
    {
        return $this->invoiceId;
    }
}
