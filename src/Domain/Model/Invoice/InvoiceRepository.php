<?php

namespace DTL\Domain\Model\Invoice;

interface InvoiceRepository
{
    public function findById(int $id): Invoice;
}
