<?php

namespace DTL\Domain\Model\Voucher;

enum Status: int
{
    case UN_USED = 0;
    case USED = 1;
}
