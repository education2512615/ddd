<?php

namespace DTL\Domain\Model\Voucher;

use Webmozart\Assert\Assert;

class Code
{
    public const MIN_LEN = 6;

    private function __construct(private readonly string $value)
    {
        Assert::notNull($value);
        Assert::minLength($value, self::MIN_LEN);
    }

    public static function fromString(?string $value): self
    {
        return new self($value);
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }
}
