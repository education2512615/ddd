<?php

namespace DTL\Domain\Model\Voucher;

use DTL\Domain\Model\Order\Order;

interface VoucherRepository
{
    public function findByCode(string $code): Voucher;

    public function updateVoucherStatus(Order $order): void;
}
