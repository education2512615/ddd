<?php

namespace DTL\Domain\Model\Voucher;

enum ScopeVoucher: int
{
    case ITEM = 1;
    case ORDER = 2;
}
