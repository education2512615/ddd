<?php

namespace DTL\Domain\Model\Voucher;

use Webmozart\Assert\Assert;

class Cash
{
    private function __construct(private readonly float $value)
    {
        Assert::float($value);
    }

    public static function fromString(float $value): self
    {
        return new self($value);
    }

    /**
     * @return float
     */
    public function getValue(): float
    {
        return $this->value;
    }
}
