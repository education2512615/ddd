<?php

namespace DTL\Domain\Model\Voucher;

class Voucher
{
    private function __construct(
        private readonly Code $code,
        private readonly Cash $cash,
        private readonly string $expiredAt,
        private readonly ScopeVoucher $scopeVoucher,
        private readonly Status $status
    ) {
    }

    public static function fromPrimitives(
        Code $code,
        Cash $cash,
        string $expiredAt,
        ScopeVoucher $scopeVoucher,
        Status $status
    ): self {
        return new self($code, $cash, $expiredAt, $scopeVoucher, $status);
    }

    /**
     * @return Cash
     */
    public function getCash(): Cash
    {
        return $this->cash;
    }

    /**
     * @return Code
     */
    public function getCode(): Code
    {
        return $this->code;
    }

    /**
     * @return Status
     */
    public function getStatus(): Status
    {
        return $this->status;
    }


    /**
     * @return string
     */
    public function getExpiredAt(): string
    {
        return $this->expiredAt;
    }

    /**
     * @return ScopeVoucher
     */
    public function getScopeVoucher(): ScopeVoucher
    {
        return $this->scopeVoucher;
    }
}
