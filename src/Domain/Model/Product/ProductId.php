<?php

namespace DTL\Domain\Model\Product;

use Webmozart\Assert\Assert;

class ProductId
{
    private function __construct(
        private readonly int $value
    ) {
        Assert::integer($value, "Mã sản phẩm không hợp lệ");
    }

    public static function fromInt(int $value): self
    {
        return new self($value);
    }

    /**
     * @return int
     */
    public function getValue(): int
    {
        return $this->value;
    }
}
