<?php

namespace DTL\Domain\Model\Product;

use DTL\Domain\Model\Order\Order;

interface ProductRepository
{
    public function findById(int $id): Product;

    public function updateProductStock(Order $order): void;
}
