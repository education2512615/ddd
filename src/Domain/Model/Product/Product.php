<?php

namespace DTL\Domain\Model\Product;

class Product
{
    public Price $price;
    public int $stock;

    private function __construct(
        private readonly ProductId $productId
    ) {
    }

    public static function fromValue(ProductId $productId): self
    {
        return new self($productId);
    }

    /**
     * @return ProductId
     */
    public function getProductId(): ProductId
    {
        return $this->productId;
    }

    /**
     * @return Price
     */
    public function getPrice(): Price
    {
        return $this->price;
    }

    /**
     * @param Price $price
     */
    public function setPrice(Price $price): void
    {
        $this->price = $price;
    }

    /**
     * @return int
     */
    public function getStock(): int
    {
        return $this->stock;
    }

    /**
     * @param int $stock
     */
    public function setStock(int $stock): void
    {
        $this->stock = $stock;
    }
}
