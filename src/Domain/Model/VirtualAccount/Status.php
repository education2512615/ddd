<?php

namespace DTL\Domain\Model\VirtualAccount;

enum Status: int
{
    case VALID = 1;
    case INVALID = 0;
}
