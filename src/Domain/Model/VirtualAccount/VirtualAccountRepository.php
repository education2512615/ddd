<?php

namespace DTL\Domain\Model\VirtualAccount;

interface VirtualAccountRepository
{
    public function findByVirtualAccountNumber(string $virtualNumber): VirtualAccount;

    public function save(VirtualAccount $virtualAccount): void;
}
