<?php

namespace DTL\Domain\Model\VirtualAccount;

use Webmozart\Assert\Assert;

class VirtualAccountNumber
{
    public const MIN_LEN = 10;
    public const MAX_LEN = 24;
    public const START_WITH = 'MTOP';

    private function __construct(private readonly string $value)
    {
        Assert::notNull($value, 'Số tài khoản không được để trống');
        Assert::minLength($value, self::MIN_LEN);
        Assert::maxLength($value, self::MAX_LEN);
        Assert::startsWith($value, self::START_WITH);
    }

    public static function fromString(?string $value): self
    {
        return new self($value);
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }


}
