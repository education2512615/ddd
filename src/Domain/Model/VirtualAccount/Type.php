<?php

namespace DTL\Domain\Model\VirtualAccount;

enum Type: int
{
    case TECHCOMBANK = 1;
    case MBBANK = 2;
}
