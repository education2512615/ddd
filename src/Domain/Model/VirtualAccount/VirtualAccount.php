<?php

namespace DTL\Domain\Model\VirtualAccount;

use DTL\Domain\Model\Customer\Customer;
use DTL\Domain\Model\Invoice\Invoice;

class VirtualAccount
{
    private function __construct(
        private readonly Customer $customer,
        private readonly VirtualAccountNumber $virtualAccountNumber,
        private readonly Invoice $invoice,
        private readonly Type $type,
        private readonly Status $status
    ) {
    }

    public static function fromPrimitives(
        Customer $customer,
        VirtualAccountNumber $virtualAccountNumber,
        Invoice $invoice,
        Type $type,
        Status $status
    ): self {
        return new self($customer, $virtualAccountNumber, $invoice, $type, $status);
    }

    /**
     * @return Invoice
     */
    public function getInvoice(): Invoice
    {
        return $this->invoice;
    }

    /**
     * @return Type
     */
    public function getType(): Type
    {
        return $this->type;
    }

    /**
     * @return VirtualAccountNumber
     */
    public function getVirtualAccountNumber(): VirtualAccountNumber
    {
        return $this->virtualAccountNumber;
    }

    /**
     * @return Customer
     */
    public function getCustomer(): Customer
    {
        return $this->customer;
    }

    /**
     * @return Status
     */
    public function getStatus(): Status
    {
        return $this->status;
    }

}
