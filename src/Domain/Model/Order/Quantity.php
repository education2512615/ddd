<?php

namespace DTL\Domain\Model\Order;

use Webmozart\Assert\Assert;

class Quantity
{
    public const MIN = 1;

    private function __construct(
        private readonly int $value
    ) {
        Assert::greaterThanEq($value, self::MIN);
    }

    public static function fromInt(?int $value): self
    {
        return new self($value);
    }

    /**
     * @return int
     */
    public function getValue(): int
    {
        return $this->value;
    }
}
