<?php

namespace DTL\Domain\Model\Order;

interface OrderRepository
{
    public function create(Order $order): void;
}
