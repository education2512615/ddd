<?php

namespace DTL\Domain\Model\Order;

use DTL\Domain\Model\Customer\Customer;
use DTL\Domain\Model\Voucher\Voucher;

class Order
{
    public ?int $id;

    public ?Voucher $orderVoucher;

    private function __construct(
        private readonly Customer $customer,
        /**
         * @var OrderItem[]
         */
        private readonly array $orderItems,
        private readonly Note $note,
        private readonly VAT $vat
    ) {
    }

    public static function fromPrimitives(Customer $customer, array $orderItems, Note $note, VAT $vat): self
    {
        return new self($customer, $orderItems, $note, $vat);
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return Customer
     */
    public function getCustomer(): Customer
    {
        return $this->customer;
    }

    /**
     * @return array
     */
    public function getOrderItems(): array
    {
        return $this->orderItems;
    }

    /**
     * @return Note
     */
    public function getNote(): Note
    {
        return $this->note;
    }

    /**
     * @return VAT
     */
    public function getVat(): VAT
    {
        return $this->vat;
    }

    /**
     * @return Voucher|null
     */
    public function getOrderVoucher(): ?Voucher
    {
        return $this->orderVoucher;
    }

    /**
     * @param Voucher|null $orderVoucher
     */
    public function setOrderVoucher(?Voucher $orderVoucher): void
    {
        $this->orderVoucher = $orderVoucher;
    }
}
