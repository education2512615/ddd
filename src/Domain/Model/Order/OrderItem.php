<?php

namespace DTL\Domain\Model\Order;

use DTL\Domain\Model\Product\Product;
use DTL\Domain\Model\Voucher\Voucher;

class OrderItem
{
    public ?Voucher $productVoucher;

    private function __construct(
        private readonly Product $product,
        private readonly Quantity $quantity
    ) {
    }

    public static function fromValues(Product $product, Quantity $quantity): self
    {
        return new self($product, $quantity);
    }

    /**
     * @return Product
     */
    public function getProduct(): Product
    {
        return $this->product;
    }

    /**
     * @return Quantity
     */
    public function getQuantity(): Quantity
    {
        return $this->quantity;
    }

    /**
     * @return Voucher|null
     */
    public function getProductVoucher(): ?Voucher
    {
        return $this->productVoucher;
    }

    /**
     * @param Voucher|null $productVoucher
     */
    public function setProductVoucher(?Voucher $productVoucher): void
    {
        $this->productVoucher = $productVoucher;
    }
}
