<?php

namespace DTL\Domain\Model\Order;

enum VAT: int
{
    case VAT_2022 = 8;
    case VAT_2023 = 10;
}
