<?php

namespace DTL\Domain\Model\Cart;

use Webmozart\Assert\Assert;

class Note
{
    public const MIN_LEN = 1;
    public const MAX_LEN = 63000;

    private function __construct(private readonly string $value)
    {
        Assert::minLength($value, self::MIN_LEN);
        Assert::maxLength($value, self::MAX_LEN);
    }

    public static function fromString(string $value): self
    {
        return new self($value);
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

}
