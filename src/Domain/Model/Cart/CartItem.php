<?php

namespace DTL\Domain\Model\Cart;

use DTL\Domain\Model\Product\Product;

class CartItem
{
    public ?string $itemVoucher;

    private function __construct(
        private readonly Product $product,
        private Quantity $quantity
    ) {
    }

    public static function fromValues(Product $product, Quantity $quantity): self
    {
        return new self($product, $quantity);
    }

    /**
     * @return Product
     */
    public function getProduct(): Product
    {
        return $this->product;
    }

    /**
     * @return Quantity
     */
    public function getQuantity(): Quantity
    {
        return $this->quantity;
    }

    /**
     * @return string|null
     */
    public function getItemVoucher(): ?string
    {
        return $this->itemVoucher;
    }

    /**
     * @param string|null $itemVoucher
     */
    public function setItemVoucher(?string $itemVoucher): void
    {
        $this->itemVoucher = $itemVoucher;
    }

    /**
     * @param Quantity $quantity
     */
    public function setQuantity(Quantity $quantity): void
    {
        $this->quantity = $quantity;
    }
}
