<?php

namespace DTL\Domain\Model\Cart;

interface CartRepository
{
    public function create(Cart $cart): void;

    public function update(Cart $cart): void;

    public function delete(Cart $cart): void;

    public function findById(int $productId, int $customerId): Cart;

    public function order(Cart $cart): void;
}
