<?php

namespace DTL\Domain\Model\Cart;

use DTL\Domain\Model\Customer\Customer;

class Cart
{
    public ?string $voucher;

    private function __construct(
        private readonly Customer $customer,
        /**
         * @var CartItem[]
         */
        private readonly array $cartItems,
        private readonly Note $note
    ) {
    }

    public static function fromPrimitives(Customer $customer, array $cartItems, Note $note): self
    {
        return new self($customer, $cartItems, $note);
    }

    /**
     * @return array
     */
    public function getCartItems(): array
    {
        return $this->cartItems;
    }

    /**
     * @return Customer
     */
    public function getCustomer(): Customer
    {
        return $this->customer;
    }

    /**
     * @return Note
     */
    public function getNote(): Note
    {
        return $this->note;
    }

    /**
     * @return string|null
     */
    public function getVoucher(): ?string
    {
        return $this->voucher;
    }

    /**
     * @param string|null $voucher
     */
    public function setVoucher(?string $voucher): void
    {
        $this->voucher = $voucher;
    }
}
