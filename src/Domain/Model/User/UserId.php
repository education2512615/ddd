<?php

namespace DTL\Domain\Model\User;

class UserId
{
    public function __construct(private readonly int $id)
    {
    }

    public function getId(): int
    {
        return $this->id;
    }
}
