<?php

namespace DTL\Domain\Model\User;

class User
{
    private function __construct(
        private readonly UserId $userId,
        private readonly UserName $userName,
        private readonly Email $email,
        private readonly BirthDate $birthDate
    ) {
    }

    public static function fromPrimitives(
        UserId $userId,
        UserName $userName,
        Email $email,
        BirthDate $birthDate
    ): self {
        return new self($userId, $userName, $email, $birthDate);
    }

    public function getUserName(): UserName
    {
        return $this->userName;
    }

    public function getEmail(): Email
    {
        return $this->email;
    }

    public function getBirthDate(): BirthDate
    {
        return $this->birthDate;
    }

    public function getUserId(): UserId
    {
        return $this->userId;
    }

    public function toArray(): array
    {
        return [
            'id' => $this->getUserId()->getId(),
            'username' => $this->getUserName()->getValue(),
            'email' => $this->getEmail()->getValue(),
            'birthdate' => $this->getBirthDate()->getValue()
        ];
    }
}
