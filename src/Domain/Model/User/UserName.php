<?php

namespace DTL\Domain\Model\User;

use Webmozart\Assert\Assert;

class UserName
{
    private function __construct(private readonly ?string $value)
    {
        Assert::notNull($value);
    }

    public static function fromString(?string $value): self
    {
        return new self($value);
    }


    public function getValue(): string
    {
        return $this->value;
    }
}
