<?php

namespace DTL\Domain\Model\User;

use Carbon\Carbon;
use Carbon\Exceptions\InvalidDateException;
use Webmozart\Assert\Assert;

class BirthDate
{
    private function __construct(private readonly ?string $value)
    {
        Assert::notNull($value);
    }

    public static function fromValue(?string $value): self
    {
        try {
            $date = Carbon::parse($value);
        } catch (InvalidDateException) {
            throw new \InvalidArgumentException('Không đúng định dạng date');
        }

        return new self($date);
    }


    public function getValue(): string
    {
        return $this->value;
    }
}
