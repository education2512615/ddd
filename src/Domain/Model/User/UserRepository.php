<?php

namespace DTL\Domain\Model\User;

interface UserRepository
{
    public function getById(int $id): User;
}
