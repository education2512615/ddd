<?php

namespace DTL\Domain\Model\User;

use Webmozart\Assert\Assert;

class Email
{
    private function __construct(private readonly ?string $value)
    {
        Assert::notNull($value);
        Assert::email($value);
    }

    public static function fromString(?string $value): self
    {
        return new self($value);
    }


    public function getValue(): string
    {
        return $this->value;
    }
}
