<?php

namespace DTL\Domain\Model\Inquiry;

use DTL\Domain\Model\Inquiry\Detail\Detail;
use DTL\Domain\Model\VirtualAccount\VirtualAccount;

class Inquiry
{
    private function __construct(private readonly VirtualAccount $virtualAccount, private readonly Detail $detail)
    {
    }

    public static function fromPrimitives(VirtualAccount $virtualAccount, Detail $detail): self
    {
        return new  self($virtualAccount, $detail);
    }

    /**
     * @return VirtualAccount
     */
    public function getVirtualAccount(): VirtualAccount
    {
        return $this->virtualAccount;
    }

    /**
     * @return Detail
     */
    public function getDetail(): Detail
    {
        return $this->detail;
    }
}
