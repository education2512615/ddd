<?php

namespace DTL\Domain\Model\Inquiry\Detail;

class Detail
{
    private function __construct(
        private readonly MessageId $messageId,
        private readonly CreatedDate $createdDate,
        private readonly Sender $sender,
        private readonly Receiver $receiver
    ) {
    }

    public static function fromValues(
        MessageId $messageId,
        CreatedDate $createdDate,
        Sender $sender,
        Receiver $receiver
    ): self {
        return new self($messageId, $createdDate, $sender, $receiver);
    }

    /**
     * @return MessageId
     */
    public function getMessageId(): MessageId
    {
        return $this->messageId;
    }

    /**
     * @return CreatedDate
     */
    public function getCreatedDate(): CreatedDate
    {
        return $this->createdDate;
    }

    /**
     * @return Sender
     */
    public function getSender(): Sender
    {
        return $this->sender;
    }

    /**
     * @return Receiver
     */
    public function getReceiver(): Receiver
    {
        return $this->receiver;
    }
}
