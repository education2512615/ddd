<?php

namespace DTL\Domain\Model\Inquiry\Detail;

use Webmozart\Assert\Assert;

class Receiver
{
    public const MIN_LEN = 20;

    private function __construct(private readonly string $value)
    {
        Assert::minLength($value, self::MIN_LEN);
    }

    public static function fromString(string $value): self
    {
        return new self($value);
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }
}
