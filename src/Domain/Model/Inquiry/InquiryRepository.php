<?php

namespace DTL\Domain\Model\Inquiry;

interface InquiryRepository
{
    public function checkInquiryInfo(Inquiry $inquiry): bool;
}
