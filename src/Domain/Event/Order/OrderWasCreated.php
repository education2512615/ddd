<?php

namespace DTL\Domain\Event\Order;

use DTL\Domain\Model\Order\Order;

class OrderWasCreated
{
    public function __construct(private readonly Order $order)
    {
    }
}
