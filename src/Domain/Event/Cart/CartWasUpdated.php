<?php

namespace DTL\Domain\Event\Cart;

use DTL\Domain\Model\Cart\Cart;

class CartWasUpdated
{
    public function __construct(private readonly Cart $cart)
    {
    }
}
