<?php

namespace DTL\Domain\Event\Cart;

use DTL\Domain\Model\Cart\Cart;

class CartWasDeleted
{
    public function __construct(private readonly Cart $cart)
    {
    }
}
