<?php

namespace DTL\Domain\Event\Cart;

use DTL\Domain\Model\Cart\Cart;

class CartWasCreated
{
    public function __construct(private readonly Cart $cart)
    {
    }
}
