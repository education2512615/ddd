<?php

namespace DTL\Domain\Event\VirtualAccount;

use DTL\Domain\Model\VirtualAccount\VirtualAccount;

class VirtualAccountWasCreated
{
    public function __construct(private readonly VirtualAccount $virtualAccount)
    {
    }
}
