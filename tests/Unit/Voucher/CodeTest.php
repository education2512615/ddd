<?php

namespace Tests\Unit\Voucher;

use DTL\Domain\Model\Voucher\Code;
use Illuminate\Support\Str;
use Tests\TestCase;

class CodeTest extends TestCase
{
    public function test_build_code_with_null_will_throw_exception(): void
    {
        $this->expectException(\InvalidArgumentException::class);

        Code::fromString(null);
    }

    public function test_build_code_with_invalid_min_length_will_throw_exception(): void
    {
        $this->expectException(\InvalidArgumentException::class);

        Code::fromString('abc');
    }

    public function test_build_code_with_min_length_will_throw_exception(): void
    {
        $strCode = Str::random(6);
        $code = Code::fromString($strCode);
        self::assertEquals($strCode, $code->getValue());
    }

    public function test_valid_code(): void
    {
        $strCode = Str::random();
        $code = Code::fromString($strCode);
        self::assertEquals($strCode, $code->getValue());
    }
}
