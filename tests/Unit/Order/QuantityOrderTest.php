<?php

namespace Tests\Unit\Order;

use DTL\Domain\Model\Order\Quantity;
use Tests\TestCase;

class QuantityOrderTest extends TestCase
{
    public function test_build_quantity_with_invalid_min_will_throw_exception(): void
    {
        $this->expectException(\InvalidArgumentException::class);

        Quantity::fromInt(0);
    }

    public function test_build_quantity_with_min_value(): void
    {
        $quantity = Quantity::fromInt(1);

        $this->assertEquals(1, $quantity->getValue());
    }

    public function test_valid_quantity(): void
    {
        $quantity = Quantity::fromInt(value: 100);
        self::assertEquals(100, $quantity->getValue());
    }
}
