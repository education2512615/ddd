<?php

namespace Tests\Unit\Order;

use DTL\Application\Order\Command\CreateOrderCommand;
use DTL\Application\Order\Command\CreateOrderHandler;
use DTL\Application\Order\DTO\OrderItemInfo;
use DTL\Domain\Event\Order\OrderWasCreated;
use DTL\Domain\Model\Customer\Customer;
use DTL\Domain\Model\Customer\CustomerId;
use DTL\Domain\Model\Customer\CustomerRepository;
use DTL\Domain\Model\Order\OrderRepository;
use DTL\Domain\Model\Product\Product;
use DTL\Domain\Model\Product\ProductId;
use DTL\Domain\Model\Product\ProductRepository;
use DTL\Domain\Model\Voucher\Cash;
use DTL\Domain\Model\Voucher\Code;
use DTL\Domain\Model\Voucher\ScopeVoucher;
use DTL\Domain\Model\Voucher\Status;
use DTL\Domain\Model\Voucher\Voucher;
use DTL\Domain\Model\Voucher\VoucherRepository;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Str;
use Mockery;
use Mockery\MockInterface;
use Tests\TestCase;

class CreateOrderTest extends TestCase
{
    public function test_create_order(): void
    {
        Event::fake([
            OrderWasCreated::class,
        ]);

        Event::assertDispatched(OrderWasCreated::class);

        $productVoucher = Str::random(6);

        $orderItemInfos[] = new OrderItemInfo(
            productId: 1,
            quantity: 100,
            voucher: $productVoucher
        );

        $orderVoucher = Str::random(8);

        $command = new CreateOrderCommand(
            customerId: 1,
            orderItems: $orderItemInfos,
            note: 'KH mới',
            orderVoucher: $orderVoucher
        );

        $customer = Customer::fromValue(CustomerId::fromInt(value: 1));

        $customerRepository = Mockery::mock(
            CustomerRepository::class,
            static function (MockInterface $mock) use ($customer) {
                $mock->shouldReceive('findById')->andReturn($customer);
            }
        );

        $product = Product::fromValue(ProductId::fromInt(value: 1));

        $productRepository = Mockery::mock(
            ProductRepository::class,
            static function (MockInterface $mock) use ($product) {
                $mock->shouldReceive('findById')->andReturn($product);
            }
        );

        $orderRepository = Mockery::mock(
            OrderRepository::class,
            static function (MockInterface $mock) {
                $mock->shouldReceive('create')->once();
            }
        );

        $productVoucher = Voucher::fromPrimitives(
            code: Code::fromString($productVoucher),
            cash: Cash::fromString(10000),
            expiredAt: '2023-03-01',
            scopeVoucher: ScopeVoucher::ITEM,
            status: Status::UN_USED
        );

        $orderVoucher = Voucher::fromPrimitives(
            code: Code::fromString($orderVoucher),
            cash: Cash::fromString(20000),
            expiredAt: '2023-03-01',
            scopeVoucher: ScopeVoucher::ITEM,
            status: Status::UN_USED
        );

        $voucherRepository = Mockery::mock(
            VoucherRepository::class,
            static function (MockInterface $mock) use ($orderVoucher, $productVoucher) {
                $mock->shouldReceive('findByCode')->andReturn($productVoucher);
                $mock->shouldReceive('findByCode')->andReturn($orderVoucher);
                $mock->shouldReceive('updateVoucherStatus')->once();
            }
        );

        $orderHandler = new CreateOrderHandler(
            orderRepository: $orderRepository,
            customerRepository: $customerRepository,
            productRepository: $productRepository,
            voucherRepository: $voucherRepository
        );

        $orderHandler->handle($command);

        self::assertTrue(true);
    }
}
