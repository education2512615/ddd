<?php

namespace Tests\Unit\Order;

use DTL\Domain\Model\Order\Note;
use Tests\TestCase;

class NoteTest extends TestCase
{
    public function test_note_too_short_will_throw_exception(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        Note::fromString('');
    }

    public function test_valid_note_with_min_length(): void
    {
        $requirement = Note::fromString('This is requirement');
        $this->assertEquals('This is requirement', $requirement->getvalue());
    }
}
