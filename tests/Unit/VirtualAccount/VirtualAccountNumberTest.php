<?php

namespace Tests\Unit\VirtualAccount;

use DTL\Domain\Model\VirtualAccount\VirtualAccountNumber;
use Tests\TestCase;

class VirtualAccountNumberTest extends TestCase
{
    public function test_va_number_null_will_throw_exception(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        VirtualAccountNumber::fromString(null);
    }

    public function test_va_number_too_short_will_throw_exception(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        VirtualAccountNumber::fromString('a');
    }

    public function test_too_long_va_number_will_throw_exception(): void
    {
        $this->expectException(\InvalidArgumentException::class);

        VirtualAccountNumber::fromString('Nhân Viên Dịch Vụ Khách Hàng Aeon Mall (Quận Tân Phú), Nhân Viên Dịch Vụ Khách Hàng Aeon Mall (Quận Tân Phú), Nhân Viên Dịch Vụ Khách Hàng Aeon Mall (Quận Tân Phú), Nhân Viên Dịch Vụ Khách Hàng Aeon Mall (Quận Tân Phú), Nhân Viên Dịch Vụ Khách Hàng Aeon Mail');
    }

    public function test_va_number_not_valid_start_with_will_throw_exception(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        VirtualAccountNumber::fromString('DuyThoLe');
    }

    public function test_valid_title_with_min_length(): void
    {
        $vaNumber = VirtualAccountNumber::fromString('MTOP12345098');
        $this->assertEquals('MTOP12345098', $vaNumber->getvalue());
    }

    public function test_valid_title_with_max_length(): void
    {
        $vaNumber = VirtualAccountNumber::fromString('MTOP12345098123450981234');
        $this->assertEquals('MTOP12345098123450981234', $vaNumber->getvalue());
    }
}
