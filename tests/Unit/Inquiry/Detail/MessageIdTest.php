<?php

namespace Tests\Unit\Inquiry\Detail;

use DTL\Domain\Model\Inquiry\Detail\MessageId;
use Tests\TestCase;

class MessageIdTest extends TestCase
{
    public function test_message_id_too_short_will_throw_exception(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        MessageId::fromString('a');
    }

    public function test_valid_message_id_with_min_length(): void
    {
        $messageId = MessageId::fromString('MTOP12345098123450985436754765');
        $this->assertEquals('MTOP12345098123450985436754765', $messageId->getvalue());
    }

    public function test_valid_message_id(): void
    {
        $messageId = MessageId::fromString('MTOP123450981234509854367547656456754765');
        $this->assertEquals('MTOP123450981234509854367547656456754765', $messageId->getvalue());
    }
}
