<?php

namespace Tests\Unit\Inquiry\Detail;

use DTL\Domain\Model\Inquiry\Detail\Sender;
use Tests\TestCase;

class SenderTest extends TestCase
{
    public function test_sender_too_short_will_throw_exception(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        Sender::fromString('a');
    }

    public function test_valid_sender_with_min_length(): void
    {
        $sender = Sender::fromString('MTOP1234509812345098');
        $this->assertEquals('MTOP1234509812345098', $sender->getvalue());
    }

    public function test_valid_sender(): void
    {
        $sender = Sender::fromString('MTOP12345098123450981234fdsfdsfgggergrhgdsfdsf');
        $this->assertEquals('MTOP12345098123450981234fdsfdsfgggergrhgdsfdsf', $sender->getvalue());
    }
}
