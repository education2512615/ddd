<?php

namespace Tests\Unit\Inquiry\Detail;

use DTL\Domain\Model\Inquiry\Detail\CreatedDate;
use Tests\TestCase;

class CreatedDateTest extends TestCase
{
    public function test_created_date_too_short_will_throw_exception(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        CreatedDate::fromString('a');
    }

    public function test_valid_title_with_min_length(): void
    {
        $createdDate = CreatedDate::fromString('MTOP12345098123450981234fdsfdsfgggergrhg');
        $this->assertEquals('MTOP12345098123450981234fdsfdsfgggergrhg', $createdDate->getvalue());
    }

    public function test_valid_title(): void
    {
        $createdDate = CreatedDate::fromString('MTOP12345098123450981234fdsfdsfgggergrhgdsfdsf');
        $this->assertEquals('MTOP12345098123450981234fdsfdsfgggergrhgdsfdsf', $createdDate->getvalue());
    }
}
