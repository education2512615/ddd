<?php

namespace Tests\Unit\Inquiry\Detail;

use DTL\Domain\Model\Inquiry\Detail\Receiver;
use Tests\TestCase;

class ReceiverTest extends TestCase
{
    public function test_receiver_too_short_will_throw_exception(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        Receiver::fromString('a');
    }

    public function test_valid_receiver_with_min_length(): void
    {
        $receiver = Receiver::fromString('MTOP1234509812345098');
        $this->assertEquals('MTOP1234509812345098', $receiver->getvalue());
    }

    public function test_valid_receiver(): void
    {
        $receiver = Receiver::fromString('MTOP12345098123450981234fdsfdsfgggergrhgdsfdsf');
        $this->assertEquals('MTOP12345098123450981234fdsfdsfgggergrhgdsfdsf', $receiver->getvalue());
    }
}
