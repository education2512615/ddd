<?php

namespace Tests\Unit\Product;

use DTL\Domain\Model\Product\ProductId;
use Tests\TestCase;

class ProductIdTest extends TestCase
{
    public function test_valid_quantity(): void
    {
        $quantity = ProductId::fromInt(value: 1);
        self::assertEquals(1, $quantity->getValue());
    }
}
