<?php

namespace Tests\Unit\Customer;

use DTL\Domain\Model\Customer\CustomerId;
use Tests\TestCase;

class CustomerIdTest extends TestCase
{
    public function test_valid_customer_id(): void
    {
        $customerId = CustomerId::fromInt(value: 1);
        self::assertEquals(1, $customerId->getValue());
    }
}
