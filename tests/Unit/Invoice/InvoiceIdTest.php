<?php

namespace Tests\Unit\Invoice;

use DTL\Domain\Model\Invoice\InvoiceId;
use Tests\TestCase;

class InvoiceIdTest extends TestCase
{
    public function test_valid_invoice_id(): void
    {
        $customerId = InvoiceId::fromInt(value: 1);
        self::assertEquals(1, $customerId->getValue());
    }
}
