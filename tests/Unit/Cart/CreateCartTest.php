<?php

namespace Tests\Unit\Cart;

use DTL\Application\Cart\Command\CreatCartCommand;
use DTL\Application\Cart\Command\CreateCartHandler;
use DTL\Domain\Event\Cart\CartWasCreated;
use DTL\Domain\Model\Cart\CartRepository;
use DTL\Domain\Model\Customer\Customer;
use DTL\Domain\Model\Customer\CustomerId;
use DTL\Domain\Model\Customer\CustomerRepository;
use DTL\Domain\Model\Product\Product;
use DTL\Domain\Model\Product\ProductId;
use DTL\Domain\Model\Product\ProductRepository;
use Illuminate\Support\Facades\Event;
use Mockery;
use Mockery\MockInterface;
use Tests\TestCase;

class CreateCartTest extends TestCase
{
    public function test_create_cart(): void
    {
        Event::fake([
            CartWasCreated::class,
        ]);

        Event::assertDispatched(CartWasCreated::class);

        $createCartCommand = new CreatCartCommand(
            customerId: 1,
            productId: 1,
            quantity: 100,
            note: 'KH mới mua'
        );

        $customer = Customer::fromValue(CustomerId::fromInt(value: 1));

        $customerRepository = Mockery::mock(
            CustomerRepository::class,
            static function (MockInterface $mock) use ($customer) {
                $mock->shouldReceive('findById')->andReturn($customer);
            }
        );

        $product = Product::fromValue(ProductId::fromInt(value: 1));

        $productRepository = Mockery::mock(
            ProductRepository::class,
            static function (MockInterface $mock) use ($product) {
                $mock->shouldReceive('findById')->andReturn($product);
            }
        );

        $cartRepository = Mockery::mock(
            CartRepository::class,
            static function (MockInterface $mock) {
                $mock->shouldReceive('create')->once();
            }
        );

        $cartHandler = new CreateCartHandler($cartRepository, $customerRepository, $productRepository);
        $cartHandler->handle($createCartCommand);
        self::assertTrue(true);
    }
}
