<?php

namespace Tests\Unit\Cart;

use DTL\Application\Cart\Command\CreateOrderCartCommand;
use DTL\Application\Cart\Command\CreateOrderCartHandler;
use DTL\Application\Cart\DTO\CartItemInfo;
use DTL\Domain\Event\Cart\OrderCartWasCreated;
use DTL\Domain\Model\Cart\CartRepository;
use DTL\Domain\Model\Customer\Customer;
use DTL\Domain\Model\Customer\CustomerId;
use DTL\Domain\Model\Customer\CustomerRepository;
use DTL\Domain\Model\Product\Product;
use DTL\Domain\Model\Product\ProductId;
use DTL\Domain\Model\Product\ProductRepository;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Str;
use Mockery;
use Mockery\MockInterface;
use Tests\TestCase;

class CreateCartOrderTest extends TestCase
{
    public function test_create_cart_order(): void
    {
        Event::fake([
            OrderCartWasCreated::class,
        ]);

        Event::assertDispatched(OrderCartWasCreated::class);

        $cartItemInfos[] = new CartItemInfo(
            productId: 1,
            quantity: 100,
            itemVoucher: Str::random(6)
        );

        $command = new CreateOrderCartCommand(
            customerId: 1,
            cartItems: $cartItemInfos,
            note: 'KH mới mua',
            voucher: Str::random(6)
        );

        $customer = Customer::fromValue(CustomerId::fromInt(value: 1));

        $customerRepository = Mockery::mock(
            CustomerRepository::class,
            static function (MockInterface $mock) use ($customer) {
                $mock->shouldReceive('findById')->andReturn($customer);
            }
        );

        $product = Product::fromValue(ProductId::fromInt(value: 1));

        $productRepository = Mockery::mock(
            ProductRepository::class,
            static function (MockInterface $mock) use ($product) {
                $mock->shouldReceive('findById')->andReturn($product);
            }
        );

        $cartRepository = Mockery::mock(
            CartRepository::class,
            static function (MockInterface $mock) {
                $mock->shouldReceive('create')->once();
            }
        );

        $cartHandler = new CreateOrderCartHandler(
            cartRepository: $cartRepository,
            customerRepository: $customerRepository,
            productRepository: $productRepository
        );
        $cartHandler->handle($command);
        self::assertTrue(true);
    }
}
