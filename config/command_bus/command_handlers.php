<?php

use DTL\Application\Cart\Command\CreatCartCommand;
use DTL\Application\Cart\Command\CreateCartHandler;
use DTL\Application\Cart\Command\DeleteCartCommand;
use DTL\Application\Cart\Command\DeleteCartHandler;
use DTL\Application\Cart\Command\UpdateCartCommand;
use DTL\Application\Cart\Command\UpdateCartHandler;
use DTL\Application\Inquiry\InquiryCommand;
use DTL\Application\Inquiry\InquiryHandler;
use DTL\Application\Order\Command\CreateOrderCommand;
use DTL\Application\Order\Command\CreateOrderHandler;
use DTL\Application\Cart\Command\CreateOrderCartCommand;
use DTL\Application\Cart\Command\CreateOrderCartHandler;
use DTL\Application\VirtualAccount\CreateVirtualAccountCommand;
use DTL\Application\VirtualAccount\CreateVirtualAccountHandler;

return [
    //cart
    CreatCartCommand::class => CreateCartHandler::class,
    UpdateCartCommand::class => UpdateCartHandler::class,
    DeleteCartCommand::class => DeleteCartHandler::class,
    CreateOrderCartCommand::class => CreateOrderCartHandler::class,

    //order
    CreateOrderCommand::class => CreateOrderHandler::class,

    //virtual account
    CreateVirtualAccountCommand::class => CreateVirtualAccountHandler::class,

    //inquiry
    InquiryCommand::class => InquiryHandler::class,

];

