<?php


use DTL\Application\User\GetUserByIdHandler;
use DTL\Application\User\GetUserByIdQuery;

return [
    //user
    GetUserByIdQuery::class => GetUserByIdHandler::class,
];

