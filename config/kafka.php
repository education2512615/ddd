<?php

return [
    'brokers' => env('KAFKA_BROKERS', 'localhost:9092'),
    'topics' => [
        'cart' => env('KAFKA_TOPIC_CART'),
    ],
    'schema_avro' => [
        'cart' => env('KAFKA_SCHEMA_AVRO_CART'),
    ],
    'consumer_group_id' => [
        'assign_rc' => env('KAFKA_CONSUMER_ASSIGN_RC_GROUP_ID')
    ],

    'base_uri_serializer' => env('KAFKA_SCHEMA_REGISTRY'),

    /*
     | After the consumer receives its assignment from the coordinator,
     | it must determine the initial position for each assigned partition.
     | When the group is first created, before any messages have been consumed, the position is set according to a configurable
     | offset reset policy (auto.offset.reset). Typically, consumption starts either at the earliest offset or the latest offset.
     | You can choose between "latest", "earliest" or "none".
     */
    'offset_reset' => env('KAFKA_OFFSET_RESET', 'latest'),
];
