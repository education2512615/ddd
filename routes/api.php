<?php

use App\Http\Controllers\API\V1\CartController;
use App\Http\Controllers\API\V1\OrderController;
use App\Http\Controllers\API\V1\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1'], static function () {
    Route::group(['middleware' => 'verify-token'], static function () {
        Route::group(['prefix' => 'carts'], static function () {
            Route::post('create', [CartController::class, 'store']);
            Route::post('update', [CartController::class, 'update']);
            Route::post('delete', [CartController::class, 'delete']);
            Route::post('order', [CartController::class, 'order']);
        });

        Route::group(['prefix' => 'orders'], static function () {
            Route::post('create', [OrderController::class, 'create']);
        });

        Route::group(['prefix' => 'users'], static function () {
            Route::get('detail/{id}', [UserController::class, 'detail']);
        });
    });
});
